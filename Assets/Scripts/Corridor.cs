﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Corridor
{
    public List<Room> connectedRooms { get; set; }

    public List<Vector3> tiles { get; set; }

    public int corridorID { get; set; }

    public Corridor(int id)
    {
        corridorID = id;
        connectedRooms = new List<Room>();
        tiles = new List<Vector3>();
    }

    public void AddRoom(Room room)
    {
        connectedRooms.Add(room);
        foreach (Room r in connectedRooms)
        {
            r.connectedCorridors.Add(this);
        }
    }
}
