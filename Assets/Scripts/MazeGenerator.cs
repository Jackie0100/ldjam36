﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MazeGenerator : MonoBehaviour
{
    public static string roomidentifier = "Room:";
    public static string corridoridentifier = "Corridor:";
    public static string corridoridoordentifier = "Door:";
    public static string dooridentifier = "GoalDoor:";

    [SerializeField]
    public List<Room> rooms { get; set; }
    public List<Corridor> corridors { get; set; }

    public List<Monster> spawnedMonsters { get; set; }

    [SerializeField]
    public GameObject floor;
    [SerializeField]
    public GameObject wall;
    [SerializeField]
    public GameObject roof;
    [SerializeField]
    public GameObject door;
    [SerializeField]
    public GameObject staff;
    [SerializeField]
    public GameObject spikeTrap;

    [SerializeField]
    public Material trapMat;

    [SerializeField]
    public Player player;


    [SerializeField]
    int maxRoomAmount = 30;

    [SerializeField]
    Rect dungeonSize = new Rect(-100, -100, 200, 200);

    [SerializeField]
    Rect roomSize = new Rect(6, 6, 20, 20);

    [SerializeField]
    int retries = 5;

    [SerializeField]
    public int amountOfTraps;
    public GameObject[] monster;

    public string[,] mazeRepresentation { get; set; }

    public GameObject[,,] mazeLayoutGameObjects { get; set; }

    public static MazeGenerator instance { get; private set; }

    public string this[int x, int y]
    {
        get
        {
            return mazeRepresentation[x, y];
        }
    }

    // Use this for initialization
    void Start()
    {
        instance = this;
        mazeRepresentation = new string[(int)(dungeonSize.width + roomSize.width), (int)(dungeonSize.height + roomSize.height)];
        mazeLayoutGameObjects = new GameObject[(int)(dungeonSize.width + roomSize.width), 1, (int)(dungeonSize.height + roomSize.height)];
        spawnedMonsters = new List<Monster>();
        Random.InitState(System.DateTime.Now.Second);
        Debug.Log(Random.value);
        rooms = new List<Room>();
        corridors = new List<Corridor>();

        Room currentRoom;
        bool overlaps = false;
        int tries = retries;

        int indexer = 0;


        for (indexer = 0; indexer < maxRoomAmount; indexer++)
        {
            currentRoom = new Room((int)Random.Range(dungeonSize.xMin, dungeonSize.xMax), (int)Random.Range(dungeonSize.yMin, dungeonSize.yMax), 0,
                (int)Random.Range(roomSize.x, roomSize.width), (int)Random.Range(roomSize.y, roomSize.height), 5, indexer);
            for (int j = 0; j < rooms.Count; j++)
            {
                if (rooms[j].size.Overlaps(currentRoom.offsetSize))
                {
                    overlaps = true;
                    break;
                }
            }
            if (!overlaps)
            {
                rooms.Add(currentRoom);
                currentRoom.CreateRoom();
                
                tries = retries;
            }
            else
            {
                if (tries == 0)
                {
                    tries = retries;
                }
                else
                {
                    indexer--;
                    tries--;
                }
                overlaps = false;
            }
        }

        for (indexer = 0; indexer < rooms.Count; indexer++)
        {
            Room selectedroom = rooms[Random.Range(0, rooms.Count)];
            do
            {
                if (selectedroom != rooms[indexer])
                {
                    corridors.Add(rooms[indexer].ConnectRoom(selectedroom, indexer));
                }
            }
            while ((selectedroom = rooms[Random.Range(0, rooms.Count)]) == rooms[indexer]);
        }
        List<Room> unVisited = (List<Room>)rooms.ToList<Room>();

        do
        {
            unVisited = (List<Room>)rooms.ToList<Room>();

            rooms[0].DepthFirstSearch(ref unVisited);

            foreach (Room r in unVisited)
            {
                corridors.Add(r.ConnectRoom(rooms[Random.Range(0, rooms.Count)], indexer++));
            }
        }
        while (unVisited.Count != 0);

        foreach (Room r in unVisited)
        {
            Debug.Log(r.roomID);
        }

        Transform goParent;
        GameObject gofloor;
        GameObject gowall;
        GameObject goroof;
        GameObject godoor;
        GameObject gostaff;
        GameObject gospikeTrap;

        for (int x = 0; x < mazeRepresentation.GetLength(0); x++)
        {
            for (int z = 0; z < mazeRepresentation.GetLength(1); z++)
            {
                if (mazeRepresentation[x, z] != null)
                {
                    goParent = this.transform.FindChild(mazeRepresentation[x, z]);
                    if(goParent == null)
                    {
                        goParent = new GameObject(mazeRepresentation[x, z]).transform;
                        goParent.SetParent(this.transform);
                    }

                    gofloor = GameObject.Instantiate<GameObject>(MazeGenerator.instance.floor);
                    gofloor.transform.SetParent(this.transform);
                    gofloor.transform.position = new Vector3(x, 0, z);
                    gofloor.name = "floor";

                    SetGameObject(x, 0, z, gofloor);

                    gofloor.transform.SetParent(goParent);

                    mazeLayoutGameObjects[x, 0, z] = gofloor;

                    goroof = GameObject.Instantiate<GameObject>(MazeGenerator.instance.roof);
                    goroof.transform.SetParent(gofloor.transform);
                    goroof.transform.localPosition = new Vector3(0, 0, -2);

                    goroof.transform.eulerAngles = new Vector3(-90, 0, 0);
                    goroof.name = "roof";

                    if (!(x != 0 && (mazeRepresentation[x - 1, z] != null)))
                    {

                    	CreateObject(x, 0, z, new Vector3(-0.5f, 0, -1), new Vector3(0, -90, 0), "wall:x-", wall);

                    }

                    if (!(x != mazeRepresentation.GetLength(0) - 1 && (mazeRepresentation[x + 1, z] != null)))
                    {
                        CreateObject(x, 0, z, new Vector3(0.5f, 0, -1), new Vector3(0, 90, 0), "wall:x+", wall);
                    }
                    if (!(z != 0 && mazeRepresentation[x, z - 1] != null))
                    {
                        CreateObject(x, 0, z, new Vector3(0, -0.5f, -1), new Vector3(0, 180, 0), "wall:y-", wall);
                    }

                    if (!(z != mazeRepresentation.GetLength(1) - 1 && mazeRepresentation[x, z + 1] != null))
                    {
                        CreateObject(x, 0, z, new Vector3(0, 0.5f, -1), new Vector3(0, 0, 0), "wall:y+", wall);
                    }
                }
            }
        }

        /*GameObject.Find("StaffOfPain").transform.position = new Vector3(rooms[rng].size.center.x, 1, rooms[rng].size.center.y);

            rng = Random.Range(0, rooms.Count);
            GameObject goDoor = GameObject.Find("Door");
            float xDoor = (int)rooms[rng].size.xMax - (int)rooms[rng].size.xMin;
            float yDoor = (int)rooms[rng].size.yMax - (int)rooms[rng].size.yMin;

            GameObject.Find("Door").transform.position = new Vector3(rooms[rng].size.xMax-1f, rooms[rng].floorLevel+1, rooms[rng].size.yMin-0.5f);
        */
        int rng = Random.Range(0, rooms.Count);
        player.transform.position = new Vector3(rooms[rng].size.center.x, 1, rooms[rng].size.center.y);

        // create staff
        rng = Random.Range(0, rooms.Count);
        gostaff = GameObject.Instantiate<GameObject>(MazeGenerator.instance.staff);
        gostaff.transform.localPosition = new Vector3(rooms[rng].size.xMax - 1f, rooms[rng].floorLevel + 1, rooms[rng].size.yMin);
        
        gostaff.name = "staff";

        rng = Random.Range(0, rooms.Count);
        // create door
        godoor = GameObject.Instantiate<GameObject>(MazeGenerator.instance.door);
        godoor.transform.localPosition = new Vector3(rooms[rng].size.xMax - 1f, rooms[rng].floorLevel + 1, rooms[rng].size.yMin - 0.5f);
        
        godoor.name = "door";
        godoor.GetComponent<Door>().win = GameObject.Find("Win");

        //spawn traps in rooms
        int randomRoom;
        for(int i = 0; i < amountOfTraps; i++)
        {
            randomRoom = Random.Range(0, rooms.Count);

            Vector3 vecpos = new Vector3(Mathf.FloorToInt(Random.Range(rooms[randomRoom].size.xMin + 1, rooms[randomRoom].size.xMax - 1)),
                rooms[randomRoom].floorLevel - 1.5f, Mathf.FloorToInt(Random.Range(rooms[randomRoom].size.yMin + 1, rooms[randomRoom].size.yMax - 1)));

            gospikeTrap = GameObject.Instantiate<GameObject>(MazeGenerator.instance.spikeTrap);
            gospikeTrap.transform.localPosition = vecpos;
            GetGameObject((int)gospikeTrap.transform.position.x, 0, (int)gospikeTrap.transform.position.z).GetComponent<MeshRenderer>().material = trapMat;
        }
    }

    void Update()
    {
        if (!player.hasWon)
        {
            if (!player.hasStaff && spawnedMonsters.Count != 1)
            {
                SpawnMonster();
            }
            else if (player.hasStaff && spawnedMonsters.Count != 2)
            {
                SpawnMonster();
            }
        }
    }

    void CreateObject(int x, int y, int z, Vector3 pos, Vector3 rot, string name, GameObject instantiateobject)
    {
        GameObject go;

        go = GameObject.Instantiate<GameObject>(instantiateobject);
        go.name = name;
        go.transform.SetParent(mazeLayoutGameObjects[x, y, z].transform);
        go.transform.localPosition = pos;
        go.transform.eulerAngles = rot;
    }

    public GameObject GetGameObject(int x, int y, int z)
    {
        return mazeLayoutGameObjects[x, y, z];
    }

    public void SetGameObject(int x, int y, int z, GameObject go)
    {
        mazeLayoutGameObjects[x, y, z] = go;
    }

    public void SetRepresentation(int x, int y, string str)
    {
        try
        {
            mazeRepresentation[x - (int)dungeonSize.x, y - (int)dungeonSize.y] = str;
        }
        catch
        {
            Debug.Log((x - (int)dungeonSize.x) + " " + (y - (int)dungeonSize.y));
        }
    }

    public string GetRepresentation(int x, int y)
    {
        try
        {
            return mazeRepresentation[x - (int)dungeonSize.x, y - (int)dungeonSize.y];
        }
        catch
        {
            Debug.Log((x - (int)dungeonSize.x) + " " + (y - (int)dungeonSize.y));
        }
        return "";
    }

    public void SpawnMonster()
    {
        Vector2 vec = rooms[Random.Range(0, rooms.Count)].size.center;
        if (Vector2.Distance(new Vector2(player.transform.position.x, player.transform.position.z), vec) >= 5)
        {
            GameObject goMon = GameObject.Instantiate<GameObject>(monster[Random.Range(0, monster.Length)]);
            goMon.transform.position = new Vector3(vec.x, 1, vec.y);
            spawnedMonsters.Add(goMon.GetComponent<Monster>());
        }
    }
}
