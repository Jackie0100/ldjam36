﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Room
{
    public bool hasDoor = false;

    public int roomID { get; set; }
    public Rect size { get; set; }
    public int floorLevel { get; set; }
    public int roomOffset { get; set; }
    public bool HasDoor { get; set; }
    public Door Door { get; set; }

    public Color color { get; set; }
    
    public List<Corridor> connectedCorridors { get; set; }

    public Rect offsetSize
    {
        get
        {
            return new Rect(size.x - roomOffset, size.y - roomOffset, size.width + (roomOffset * 2), size.height + (roomOffset * 2));
        }
    }

    public Room(int x, int y, int z, int width, int height, int offset, int id)
    {
        size = new Rect(x, y, width, height);
        floorLevel = z;
        roomOffset = offset;
        roomID = id;
        connectedCorridors = new List<Corridor>();
    }

    public void CreateRoom()
    {
        for (int i = 0; i < size.width; i++)
        {
            for (int j = 0; j < size.height; j++)
            {
                MazeGenerator.instance.SetRepresentation((int)size.x + i, (int)size.y + j, MazeGenerator.roomidentifier + roomID);
            }
        }
    }

    public Corridor ConnectRoom(Room room, int corridorid)
    {
        Corridor cor = new Corridor(corridorid);
        Color col = Random.ColorHSV();
        
        if (room.size.center.x < size.center.x)
        {
            for (int i = 0; i <= Mathf.FloorToInt(size.center.x - Mathf.FloorToInt(room.size.center.x)); i++)
            {
                if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x - i), Mathf.FloorToInt(size.center.y)) == null)
                {
                    CreateCorridor(new Vector3(Mathf.Floor(size.center.x - i), floorLevel, Mathf.Floor(size.center.y)), corridorid);
                    cor.tiles.Add(new Vector3(Mathf.Floor(size.center.x - i), floorLevel, Mathf.Floor(size.center.y)));
                }
                else if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x - i), Mathf.FloorToInt(size.center.y)).Contains(MazeGenerator.corridoridentifier) &&
                    MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x - i), Mathf.FloorToInt(room.size.center.y)) != (MazeGenerator.corridoridentifier + corridorid))
                {
                    Corridor colcor = MazeGenerator.instance.corridors.Find(c => c.corridorID.ToString() ==
                    MazeGenerator.instance.GetRepresentation((int)size.center.x - i, (int)size.center.y).Substring(MazeGenerator.corridoridentifier.Length));

                    cor.AddRoom(this);
                    colcor.AddRoom(this);

                    return cor;
                }
                else if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x - i), Mathf.FloorToInt(size.center.y)).Contains(MazeGenerator.roomidentifier) &&
                    (!(MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x - i), Mathf.FloorToInt(size.center.y)).Contains(roomID.ToString())) &&
                    !(MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x - i), Mathf.FloorToInt(size.center.y)).Contains(room.roomID.ToString()))))
                {
                    cor.AddRoom(FindRoom(new Vector2(size.center.x - i, size.center.y)));
                    cor.AddRoom(this);

                    return cor;
                }
            }
        }
        else
        {
            for (int i = 0; i <= Mathf.FloorToInt(room.size.center.x - Mathf.CeilToInt(size.center.x)); i++)
            {
                if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x + i), Mathf.FloorToInt(size.center.y)) == null)
                {
                    CreateCorridor(new Vector3(Mathf.Floor(size.center.x + i), floorLevel, Mathf.Floor(size.center.y)), corridorid);
                    cor.tiles.Add(new Vector3(Mathf.Floor(size.center.x + i), floorLevel, Mathf.Floor(size.center.y)));
                }
                else if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x + i), Mathf.FloorToInt(size.center.y)).Contains(MazeGenerator.corridoridentifier) &&
                    MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x + i), Mathf.FloorToInt(room.size.center.y)) != (MazeGenerator.corridoridentifier + corridorid))
                {
                    Corridor colcor = MazeGenerator.instance.corridors.Find(c => c.corridorID.ToString() ==
                    MazeGenerator.instance.GetRepresentation((int)size.center.x + i, (int)size.center.y).Substring(MazeGenerator.corridoridentifier.Length));

                    cor.AddRoom(this);
                    colcor.AddRoom(this);

                    return cor;
                }
                else if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x + i), Mathf.FloorToInt(size.center.y)).Contains(MazeGenerator.roomidentifier) &&
                    (!(MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x + i), Mathf.FloorToInt(size.center.y)).Contains(roomID.ToString())) &&
                    !(MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(size.center.x + i), Mathf.FloorToInt(size.center.y)).Contains(room.roomID.ToString()))))
                {
                    cor.AddRoom(FindRoom(new Vector2(size.center.x + i, size.center.y)));
                    cor.AddRoom(this);

                    return cor;
                }
            }
        }

        if (room.size.center.y < size.center.y)
        {
            for (int i = Mathf.FloorToInt(size.center.y - Mathf.FloorToInt(room.size.center.y)); i >= 0; i--)
            {
                if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y + i)) == null)
                {
                    CreateCorridor(new Vector3(Mathf.Floor(room.size.center.x), floorLevel, Mathf.Floor(room.size.center.y + i)), corridorid);
                    cor.tiles.Add(new Vector3(Mathf.Floor(size.center.x), floorLevel, Mathf.Floor(size.center.y + i)));
                }
                else if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y + i)).Contains(MazeGenerator.corridoridentifier) &&
                    MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y + i)) != (MazeGenerator.corridoridentifier + corridorid))
                {
                    Corridor colcor = MazeGenerator.instance.corridors.Find(c => c.corridorID.ToString() ==
                    MazeGenerator.instance.GetRepresentation((int)room.size.center.x, (int)room.size.center.y + i).Substring(MazeGenerator.corridoridentifier.Length));

                    cor.AddRoom(this);
                    colcor.AddRoom(this);

                    return cor;
                }
                else if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y + i)).Contains(MazeGenerator.roomidentifier) &&
                    (!(MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y + i)).Contains(roomID.ToString())) &&
                    !(MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y + i)).Contains(room.roomID.ToString()))))
                {
                    cor.AddRoom(FindRoom(new Vector2(room.size.center.x, room.size.center.y + i)));
                    cor.AddRoom(this);

                    return cor;
                }
            }
        }
        else
        {
            for (int i = Mathf.FloorToInt(room.size.center.y - Mathf.FloorToInt(size.center.y)); i >= 0; i--)
            {
                if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y - i)) == null)
                {
                    CreateCorridor(new Vector3(Mathf.Floor(room.size.center.x), floorLevel, Mathf.Floor(room.size.center.y - i)), corridorid);
                    cor.tiles.Add(new Vector3(Mathf.Floor(size.center.x), floorLevel, Mathf.Floor(size.center.y - i)));
                }
                else if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y - i)).Contains(MazeGenerator.corridoridentifier) &&
                    MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y - i)) != (MazeGenerator.corridoridentifier + corridorid))
                {
                    Corridor colcor = MazeGenerator.instance.corridors.Find(c => c.corridorID.ToString() ==
                    MazeGenerator.instance.GetRepresentation((int)room.size.center.x, (int)room.size.center.y - i).Substring(MazeGenerator.corridoridentifier.Length));

                    cor.AddRoom(this);
                    colcor.AddRoom(this);

                    return cor;
                }
                else if (MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y - i)).Contains(MazeGenerator.roomidentifier) &&
                    (!(MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y - i)).Contains(roomID.ToString())) &&
                    !(MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(room.size.center.x), Mathf.FloorToInt(room.size.center.y - i)).Contains(room.roomID.ToString()))))
                {
                    cor.AddRoom(FindRoom(new Vector2(room.size.center.x, room.size.center.y - i)));
                    cor.AddRoom(this);

                    return cor;
                }
            }
        }
        cor.AddRoom(this);
        cor.AddRoom(room);
        /*if (cor.tiles.Count != 0)
        {
            MazeGenerator.instance.SetRepresentation((int)cor.tiles[0].x, (int)cor.tiles[0].z, MazeGenerator.corridoridoordentifier);
        }
        if (cor.tiles.Count >= 2)
        {
            MazeGenerator.instance.SetRepresentation((int)cor.tiles[cor.tiles.Count - 1].x, (int)cor.tiles[cor.tiles.Count - 1].z, MazeGenerator.corridoridoordentifier);
        }*/
        return cor;
    }

    public void CreateCorridor(Vector3 position, int corridorid)
    {
        CreateCorridor(position, corridorid, Color.white);
    }

    public void CreateCorridor(Vector3 position, int corridorid, Color col)
    {
        MazeGenerator.instance.SetRepresentation((int)position.x, (int)position.z, MazeGenerator.corridoridentifier + corridorid);
    }

    public Room FindRoom(Vector2 pos)
    {
        string strID = MazeGenerator.instance.GetRepresentation(Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.y)).Substring(MazeGenerator.roomidentifier.Length);
        return MazeGenerator.instance.rooms.Find(r => r.roomID.ToString() == strID);
    }

    public Room GetRoom(int id)
    {
        return MazeGenerator.instance.rooms[id];
    }

    public List<Room> DepthFirstSearch(ref List<Room> visitednodes)
    {
        visitednodes.RemoveAll(c => c.roomID == this.roomID);
        foreach (Corridor c in connectedCorridors)
        {
            foreach (Room r in c.connectedRooms)
            {
                if (visitednodes.Contains(r))
                {
                    r.DepthFirstSearch(ref visitednodes);
                }
            }
        }
        return visitednodes;
    }
}
