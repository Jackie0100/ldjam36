﻿using UnityEngine;
using System.Collections;

public class Artifact : MonoBehaviour
{
    [SerializeField]
    float speed;

    Vector2 offset;
    Vector2 tiling;

    // Update is called once per frame
    void Update ()
    {
        tiling = new Vector2((1 + Mathf.PerlinNoise(Time.time * speed, Time.time * 0.5f * speed)), 1 + Mathf.PerlinNoise(Time.time * 0.5f * speed, Time.time * speed));
        offset += new Vector2(Time.deltaTime * speed * Random.Range(0.0f, 1.0f), Time.deltaTime * speed * Random.Range(0.0f, 1.0f));
        this.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", offset);
        this.GetComponent<Renderer>().material.SetTextureScale("_MainTex", tiling);
    }
}
