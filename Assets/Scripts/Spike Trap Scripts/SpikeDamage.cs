﻿using UnityEngine;
using System.Collections;

public class SpikeDamage : MonoBehaviour
{
    public int damage = 1;
    AudioSource audio;
    public AudioClip damageSound;

	// Use this for initialization
	void Start ()
    {
        audio = GetComponent<AudioSource>(); 
	}


    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Player")
        {
            col.gameObject.GetComponent<Player>().TakeDamage(1);
           // Debug.Log("Collision detected! Player must die");
        }
    }
}
