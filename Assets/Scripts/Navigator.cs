﻿using UnityEngine;
using System.Collections;

public class Navigator : MonoBehaviour
{
    float lifetime = 0;

	// Update is called once per frame
	void Update ()
    {
        if (lifetime >= 5)
        {
            GameObject.Destroy(this.gameObject);
        }
        if (MazeGenerator.instance.player.hasStaff)
        {
            this.transform.LookAt(GameObject.Find("door").transform);
        }
        else if (!MazeGenerator.instance.player.hasStaff)
        {
            this.transform.LookAt(GameObject.Find("staff").transform);
        }

        this.transform.Translate(Vector3.forward * Time.deltaTime);

        lifetime += Time.deltaTime;
    }
}
