﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour
{
    GameObject playerobj;
    float speed = 3;

    bool haveChasedPlayer = false;
    bool haveattacked = false;

	// Use this for initialization
	void Start ()
    {
        playerobj = GameObject.Find("Player");
        this.GetComponent<Animator>().SetBool("Run", true);

    }

    // Update is called once per frame
    void Update ()
    {
        if (MazeGenerator.instance.player.isDead || MazeGenerator.instance.player.hasWon)
        {
            return;
        }
        this.transform.LookAt(playerobj.transform);
        this.transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        if (!haveChasedPlayer && Vector3.Distance(this.transform.position, playerobj.transform.position) <= 10f)
        {
            haveChasedPlayer = true;
            Debug.Log("chasing");
        }

        if (Vector3.Distance(this.transform.position, playerobj.transform.position) > 1.5f)
        {
            this.GetComponent<Animator>().SetBool("Run", true);
            this.GetComponent<Animator>().SetBool("Atack", false);
            if (!(this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Atack_Weaponless") || 
                this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Look_Around")) && this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
            {
                this.transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }

        }
        else if (!(this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Atack_Weaponless")))
        {
            this.GetComponent<Animator>().SetBool("Atack", true);
        }

        if (haveChasedPlayer && Vector3.Distance(this.transform.position, playerobj.transform.position) >= 20f || Vector3.Distance(this.transform.position, playerobj.transform.position) >= 100f)
        {
            MazeGenerator.instance.spawnedMonsters.Remove(this);
            GameObject.Destroy(this.gameObject);
        }
    }

    public void AttackPlayer()
    {
        if (Vector3.Distance(this.transform.position, playerobj.transform.position) <= 2f)
        playerobj.GetComponent<Player>().TakeDamage(1);
    }
}
