﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour{

    public GameObject door;
    private GameObject wall;
    [SerializeField]
    public GameObject win;

    void Start()
    {
        win.SetActive(false);
    }

    void OnTriggerEnter(Collider meshCollider)
    {
        if (meshCollider.name == "Player" && meshCollider.GetComponent<Player>().hasStaff)
        {
            meshCollider.GetComponent<Player>().hasWon = true;
            win.SetActive(true);
        }
    }


    void OnMouseDown()
    {
        
    }

    public void CreateDoor()
    {
        // set a random position for the door. 
        Random.InitState(System.DateTime.Now.Second);


        // make sure that it is a wall
        // get coordinates for wall
        int roomID = Random.Range(0, MazeGenerator.instance.rooms.Count);
        Room room = new Room(1,1,1,1,1,1,1);
        room = room.GetRoom(roomID);

        MazeGenerator.instance.SetGameObject((int)room.size.xMax, (int)room.floorLevel, (int)room.size.yMax, door);
        MazeGenerator.instance.SetRepresentation((int)room.size.xMax, (int)room.floorLevel, MazeGenerator.dooridentifier);
    }

}
