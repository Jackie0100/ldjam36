﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public float walkspeed = 2f;
    public float runspeed = 4f;
    float targetspeed = 2;

    public bool isDead
    {
        get
        {
            return health <= 0;
        }
    }

    public bool hasWon;

    [SerializeField]
    private float maxhealth = 5;
    [SerializeField]
    private float _health = 5;

    private float _energy = 30;
    public float maxEnergy = 30;

    [SerializeField]
    AudioSource damageSource;
    [SerializeField]
    AudioClip hurtFx;

    [SerializeField]
    GameObject GameOverScreen;

    [SerializeField]
    GameObject fader;

    [SerializeField]
    GameObject navigator;

    [SerializeField]
    MeshRenderer healthIndicator;

    [SerializeField]
    ParticleSystem energyIndicator;


    private float timesincelastdamage = 0;


    Vector3 dirvec;

    bool runKeyReleased = true;

    public bool hasStaff;

    public float health
    {
        get
        {
            return _health;
        }

        set
        {
            timesincelastdamage = 0;
            healthIndicator.material.SetColor("_EmissionColor", new Color(0.5f, 2, 2, 2) * (value / maxhealth));
            _health = value;
        }
    }

    public float energy
    {
        get
        {
            return _energy;
        }

        set
        {
            if (value >= maxEnergy)
            {
                _energy = maxEnergy;
                return;
            }
            energyIndicator.emissionRate = (value / 100.0f * maxEnergy);
            _energy = value;
        }
    }

    // Use this for initialization
    void Start ()
    {
        damageSource = this.gameObject.AddComponent<AudioSource>();
        Cursor.lockState = CursorLockMode.Locked;
	}
	
    public void TakeDamage(int damage)
    {
        if (!damageSource.isPlaying)
        {
            damageSource.PlayOneShot(hurtFx);
        }
        health -= damage;
    }


	// Update is called once per frame
	void Update ()
    {
        timesincelastdamage += Time.deltaTime;
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
        if(hasWon)
        {
            fader.GetComponent<UnityEngine.UI.Image>().color += new Color(0, 0, 0, Time.deltaTime * 0.25f);
            Cursor.lockState = CursorLockMode.None;
        }
        if (health <= 0)
        {
            fader.GetComponent<UnityEngine.UI.Image>().color += new Color(0, 0, 0, Time.deltaTime * 0.25f);
            GameOverScreen.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKey(KeyCode.LeftShift) && energy > 0 && runKeyReleased)
        {
            targetspeed = runspeed;
            energy -= Time.deltaTime * 1.25f;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            runKeyReleased = true;
            targetspeed = walkspeed;
            energy += Time.deltaTime;
        }
        else
        {
            targetspeed = walkspeed;
            energy += Time.deltaTime;
        }
        energy = Mathf.Clamp(energy, 0, maxEnergy);
        if (energy <= 0)
        {
            runKeyReleased = false;
        }

        if(health > 0 && !hasWon)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && energy >= 5)
            {
                energy -= 5;
                GameObject go = GameObject.Instantiate<GameObject>(navigator);
                go.transform.position = this.transform.position;
            }
            float translation = Input.GetAxis("Vertical");
            float straffle = Input.GetAxis("Horizontal");

            dirvec = new Vector3(straffle, 0, translation).normalized;

            transform.Translate(dirvec * Time.deltaTime * targetspeed);
        }
    }
    
    public void RestartLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }   
}
