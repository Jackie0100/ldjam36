﻿using UnityEngine;
using System.Collections;

public class Skull : MonoBehaviour
{
    // When ever the user picks up the staff he wins

    /*public Transform onhand;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnMouseDown()
    {
        GetComponent<Rigidbody>().useGravity = false;
        this.transform.position = onhand.position;
        this.transform.parent = GameObject.Find("Main Camera").transform;

    }

    void OnMouseUp()
    {
        this.transform.parent = null;
        GetComponent<Rigidbody>().useGravity = true;
        
    }*/

    void OnTriggerEnter(Collider col)
    {
        if (col.name == "Player" && !col.GetComponent<Player>().hasStaff)
        {
            this.transform.SetParent(GameObject.Find("Main Camera").transform);
            this.transform.localPosition = new Vector3(0.3f, -0.2f, 0.4f);
            this.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            this.transform.localEulerAngles = new Vector3(0, -135, 0);
            col.GetComponent<Player>().hasStaff = true;
            Transform t = GameObject.Find("Main Camera").transform.FindChild("Torch");
            t.SetParent(null);
            t.localScale += new Vector3(0.25f, 0.25f, 0.25f);
            t.gameObject.AddComponent<CapsuleCollider>();
            t.gameObject.GetComponent<CapsuleCollider>().radius = 0.25f;
            t.gameObject.AddComponent<Rigidbody>();
            t.gameObject.GetComponent<Rigidbody>().angularDrag = 10;
            t.gameObject.GetComponent<Rigidbody>().drag = 10;
        }
    }

}
